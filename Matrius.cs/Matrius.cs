﻿/*
AUTHOR: Adrián Rodríguez Bolín
DATE: 23/11/2022
DESCRIPTION: Ejercicios de matrices 
*/

using Microsoft.VisualBasic.FileIO;
using System;
using System.Globalization;
using System.Security.Cryptography;

namespace Matrius.cs
{
    class Matrius
    {
        
        // Te indica que hay en las cordenadas que tu elijas del tablero.
        public void SimpleBattleshipResult()
        {
            char[,] tablero = { { 'x', 'x', '0', '0', '0', '0', 'x' }, { '0', '0', 'x', '0', '0', '0', 'x' }, { '0', '0', '0', '0', '0', '0', 'x' }, { '0', 'x', 'x', 'x', '0', '0', 'x' }, { '0', '0', '0', '0', 'x', '0', '0' }, { '0', '0', '0', '0', 'x', '0', '0' }, { 'x', '0', '0', '0', '0', '0', '0' }};
            Console.WriteLine("Introduce las cordenadas para disparar en formato longitud y latitud.");
            Console.WriteLine("Introduce el valor de la longitud (del 0 al 6)");
            int j = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Introduce el valor de la latitud (del 0 al 6)");
            int i = Convert.ToInt32(Console.ReadLine());
            
            if (tablero[i, j] == '0')
            {
                Console.WriteLine("Agua");
            }
            else
            {
                Console.WriteLine("Tocado!!!");
            }

            Console.WriteLine(tablero[i,j]);
        }

        // Sumar elementos de la matriz
        public void MatrixElementSum()
        {
            int[,] matriz = { { 2, 5, 1, 6 }, { 23, 52, 14, 36 }, { 23, 75, 81, 64 } };
            int sum = 0;
            for (int j = 0; j < 4; j++)
            {
                for (int i = 0; i < 3; i++)
                {
                    sum += matriz[i, j];
                }

            }
            Console.WriteLine(sum);
        }

        // Imprime una matriz que indica cuantas veces se ha abierto cada caja.
        public void MatrixBoxesOpenedCounter()
        {
            int[,] cajas = { { 0, 0, 0, 0 }, { 0, 0, 0, 0 }, { 0, 0, 0, 0 }, { 0, 0, 0, 0 } };
            int i;
            int j;
            do
            {
                
                Console.WriteLine("Introduce el primer valor de la caja (del 0 al 3). Introduce -1 para acabar.");
                j = Convert.ToInt32(Console.ReadLine());
                if (j != -1)
                {
                    Console.WriteLine("Introduce el segundo valor de la caja (del 0 al 3)");
                    i = Convert.ToInt32(Console.ReadLine());
                    cajas[i, j]++;
                }
            } while (j != -1);
            for (j = 0; j < 4; j++)
            {
                for (i = 0; i < 4; i++)
                {
                    Console.Write(cajas[i, j] + " ");
                }
                Console.WriteLine("\n");
            }
        }

        // Imprime por pantalla true si alguno de estos números es divisible entre 13
        public void MatrixThereADiv13()
        {
            int[,] matriz = { { 2, 5, 1, 6 }, { 23, 52, 14, 36 }, { 23, 75, 81, 64 } };
            int div13 = 0;
            bool resultado = false;
            for (int j = 0; j < 4; j++)
            {
                for (int i = 0; i < 3; i++)
                {
                    if (matriz[i,j]%13==0)
                    {
                        div13 = matriz[i, j];
                        resultado = true;
                    }
                }
            }
            Console.WriteLine(resultado);
        }

        // Te dice las cordenadas de la cima más alta y cuanto mide.
        public void HighestMountainOnMap()
        {
            double[,] map = { { 1.5, 1.6, 1.8, 1.7, 1.6 }, { 1.5, 2.6, 2.8, 2.7, 1.6 }, { 1.5, 4.6, 4.4, 4.9, 1.6 }, { 2.5, 1.6, 3.8, 7.7, 3.6 }, { 1.5, 2.6, 3.8, 2.7, 1.6 } };
            int i=0;
            int j=0;
            double masAlto = 0;
            int x=0;
            int y=0;

            for (i = 0; i < 5; i++)
            { 
                for (j = 0; j < 5; j++)
                {
                    if (masAlto < map[i,j])
                    {
                        masAlto = map[i,j];
                        x = i;
                        y = j;
                    }
                }
            }

            Console.WriteLine("("+x +","+ y +"):"+masAlto+" metros.");
        }

        // Te dice las cordenadas de la cima más alta y cuanto mide en pies.
        public void HighestMountainScalechange()
        {
            double[,] map = { { 1.5, 1.6, 1.8, 1.7, 1.6 }, { 1.5, 2.6, 2.8, 2.7, 1.6 }, { 1.5, 4.6, 4.4, 4.9, 1.6 }, { 2.5, 1.6, 3.8, 7.7, 3.6 }, { 1.5, 2.6, 3.8, 2.7, 1.6 } };
            int i = 0;
            int j = 0;
            double masAlto = 0;
            int x = 0;
            int y = 0;

            for (i = 0; i < 5; i++)
            {
                for (j = 0; j < 5; j++)
                {
                    if (masAlto < map[i, j])
                    {
                        masAlto = map[i, j];
                        x = i;
                        y = j;
                    }
                }
            }

            masAlto *= 3.2808;

            Console.WriteLine("(" + x + "," + y + "):" + masAlto + " pies.");
        }

        // Pedir 2 matrices al usuario y sumarlas
        public void MatrixSum()
        {
            Console.WriteLine("Cuantas filas tiene la primera matriz");
            int i = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Cuantas columnas tiene la primera matriz");
            int j = Convert.ToInt32(Console.ReadLine());
            int[,] matriz1= new int[i,j];
            for (i = 0; i < matriz1.GetLength(0); i++)
            {
                for (j = 0; j < matriz1.GetLength(1); j++)
                {
                    Console.WriteLine("Introduce un valor");
                    matriz1[i, j] = Convert.ToInt32(Console.ReadLine());
                }
            }
            Console.WriteLine("Cuantas filas tiene la segunda matriz");
            i = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Cuantas columnas tiene la segunda matriz");
            j = Convert.ToInt32(Console.ReadLine());
            int[,] matriz2 = new int [i, j];


           

            for (i = 0; i < matriz2.GetLength(0); i++)
            {
                for (j = 0; j < matriz2.GetLength(1); j++)
                {
                    Console.WriteLine("Introduce un valor");

                    matriz2[i, j] = Convert.ToInt32(Console.ReadLine());
                }
            }

            for (i = 0; i < matriz1.GetLength(0); i++)
            {
                for (j = 0; j < matriz1.GetLength(1); j++)
                {
                    matriz1[i, j] += matriz2[i,j];
                }
            }

            for (i = 0; i < matriz1.GetLength(0); i++)
            {
                for (j = 0; j < matriz1.GetLength(1); j++)
                {
                    Console.Write(matriz1[i, j]+"\t");
                }
                Console.WriteLine("\n");
            }
        }

        // Escoge la posicion de la torre en un tablero de ajedrez y te muestra hacia donde la puedes mover.
        public void RookMoves()
        {
            string[,] tablero = new string[8, 8];
            int i;
            int j;
            for (i = 0; i < 8; i++)
            {
                for (j = 0; j < 8; j++)
                {
                    tablero[i, j] = "X";
                }
            }
            Console.WriteLine("En que columna del tablero quieres poner la torre? (Va de la 'a' a la 'h').");
            string opcion = Console.ReadLine();
            int columna=0;
            switch (opcion)
            {
                case "a":
                    columna = 0;
                    break;
                case "b":
                    columna = 1;
                    break;
                case "c":
                    columna = 2;  
                    break;
                case "d":
                    columna = 3;
                    break;
                case "e":
                    columna = 4;  
                    break;
                case "f":
                    columna = 5;  
                    break;
                case "g":
                    columna = 6;  
                    break;
                case "h":
                    columna = 7;
                    break;
            }
            Console.WriteLine("En que fila del tablero quieres poner la torre? (Va del 0 al 7).");
            int fila = Convert.ToInt32(Console.ReadLine());
            for (i = 0, j=columna; i < 8; i++)
            {
                tablero[i, j] = "t";
            }
            for (i = fila, j = 0; j < 8; j++)
            {
                tablero[i, j] = "t";
            }
            tablero[fila, columna]= "T";
            for (i = 0; i < 8; i++)
            {
                for (j = 0; j < 8; j++)
                {
                    Console.Write(tablero[i,j]+" ");
                }
                Console.WriteLine("\n");
            }
        }

        // Pide una matriz cuadrada y te dice si es simetrica o no.
        public void MatrixSimetric()
        {
            int filas;
            int columnas;
            do
            {
                Console.WriteLine("Cuantas filas tiene la matriz");
                filas = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Cuantas columnas tiene la matriz");
                columnas = Convert.ToInt32(Console.ReadLine());
                if(filas != columnas)
                {
                    Console.WriteLine("Las filas y las columnas deben ser iguales.");
                }
            } while (filas != columnas);
            int[,] matriz = new int[filas, columnas];
            bool resultado = true;
            for (int i = 0; i < filas; i++)
            {
                for (int j = 0; j < columnas; j++)
                {
                    Console.WriteLine("¿Que valor va el la posicion "+i+","+j+"?");    
                    matriz[i, j] = Convert.ToInt32(Console.ReadLine());
                }
            }
            for (int i = 0 ; i < filas; i++)
            {
                for (int j = 0; j < columnas; j++)
                {
                    if(matriz[i,j] != matriz[j, i])
                    {
                        resultado = false;
                        i = filas;
                        break;
                    } 
                }
            }
            if (resultado == true)
            {
                Console.WriteLine("La matriz es simetrica.");  
            }
            else
            {
                Console.WriteLine("La matriz no es simetrica.");  
            }
        }


        //EJERCICIOS REFUERZO
        // 
        public void MultiplicacioDeMatrius()
        {
               
        }

        // 
        public void QueenGame()
        {
                
        }

        //
        public void QuadratMagic()
        {

        }

        public bool Menu(bool end)
        {
            string[] exercicis = { " 0 - Exit", " 1 - SimpleBattleshipResult", " 2 - MatrixElementSum", " 3 - MatrixBoxesOpenedCounter",
            " 4 - MatrixThereDiv13", " 5 - HighestMountainOnMap", " 6 - HighestMountainScalechange", " 7 - MatrixSum",
            " 8 - RookMoves", " 9 - MatrixSimetric", "10 - MultiplicacioDeMatrius", "11 - QueenGame", "12 - QuadratMagic"};
                
            Console.WriteLine("¿Que ejercicio quieres ejecutar?\n");
            for (int i = 0; i < 13; i++)
            {
                Console.WriteLine(exercicis[i]);
            }
            Console.Write("\nEscribe un número: ");
            string option = Console.ReadLine();
            switch (option)
            {
                case "0":
                    end = !end;
                    break;
                case "1":
                    SimpleBattleshipResult();
                    break;
                case "2":
                    MatrixElementSum();
                    break;
                case "3":
                    MatrixBoxesOpenedCounter();
                    break;
                case "4":
                    MatrixThereADiv13();
                    break;
                case "5":
                    HighestMountainOnMap();
                    break;
                case "6":
                    HighestMountainScalechange();
                    break;
                case "7":
                    MatrixSum();
                    break;
                case "8":
                    RookMoves();
                    break;
                case "9":
                    MatrixSimetric();
                    break;
                case "10":
                    MultiplicacioDeMatrius();
                    break;
                case "11":
                    QueenGame();
                    break;
                case "12":
                    QuadratMagic();
                    break;
                default:
                    Console.WriteLine("\nOpción incorrecta!\n");
                    break;
            }
            Console.WriteLine("\n\n[PRESS ENTER TO CONTINUE]");
            Console.ReadLine();
            Console.Clear();
            return end;
        }                                                                                                                                       
        static void Main()
        {
            var menu = new Matrius();
            bool end = false;
            while (!end) { end = menu.Menu(end); }
        }
        
    }
}
