﻿/*
AUTHOR: Adrián Rodríguez Bolín
DATE: 16/11/2022
DESCRIPTION: Ejercicios de vectores 
*/

using System;

namespace Vectors
{
    class Vectors
    {
        // Muestra el día de la semana del 0 al 6
        public void DayOfWeek()
        {
            string[] dias = { "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado", "Domingo" };
            Console.Write("Escriu un número del 0 al 6 para escoger un día: ");
            int numdia = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine(dias[numdia]);
        }

        // Pide alineación de jugadores
        public void PlayerNumbers()
        {
            int[] alineacion = new int[5];
            Console.Write("Introduce un número de jugador: ");
            alineacion[0] = Convert.ToInt32(Console.ReadLine());
            for (int i = 1; i < 5; i++)
            {
                Console.Write("Introduotro número de jugador: ");
                alineacion[i] = Convert.ToInt32(Console.ReadLine());
            }
            Console.Write("[");
            for (int i = 0; i < alineacion.Length - 1; i++)
            {
                Console.Write(alineacion[i] + ", ");
            }
            Console.WriteLine(alineacion[alineacion.Length - 1] + "]");
        }
        // Muestra candidato respecto número de lista
        public void CandidatesList()
        {
            Console.WriteLine("Cuantos candidatos quieres?");
            int num = Convert.ToInt32(Console.ReadLine());
            string[] candidatos = new string[num];
            Console.WriteLine("A continuación introduce los nombres de los candidatos en orden:");
            for (int i = 0; i < num; i++)
            {
                candidatos[i] = Console.ReadLine();
            }
            int posicion = 0;
            while (posicion != -1)
            {
                Console.Write("¿Que número de candidato quieres? Introduce -1 para terminar: ");
                posicion = Convert.ToInt32(Console.ReadLine());
                if (posicion != -1)
                {
                    Console.WriteLine(candidatos[posicion - 1]);
                }
            }
        }
        // Imprime por pantalla la letra en X posición de una palabra
        public void LetterInWord()
        {
            Console.Write("Introduce una palabra: ");
            string pal = Console.ReadLine();
            Console.Write("Introduce un número: ");
            int num = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine(pal[num]);
        }
        // Sustituir valores en un vector
        public void AddValuesToList()
        {
            float[] vector = new float[50];
            for (int i = 0; i < 50; i++)
            {
                vector[i] = 0.0f;
            }
            vector[0] = 31.0f;
            vector[1] = 56.0f;
            vector[19] = 12.0f;
            vector[49] = 79.0f;
            Console.Write("[");
            for (int i = 0; i < 49; i++)
            {
                Console.Write(vector[i] + ".0, ");
            }
            Console.WriteLine(vector[vector.Length - 1] + ".0]");
        }
        // Cambiar el primer valor de un vector por el último
        public void Swap()
        {
            int[] vector = new int[4];
            Console.WriteLine("Introduce 4 números: ");
            for (int i = 0; i < 4; i++)
            {
                vector[i] = Convert.ToInt32(Console.ReadLine());
            }
            int otro = vector[0];
            vector[0] = vector[3];
            vector[3] = otro;
            Console.Write("[");
            for (int i = 0; i < vector.Length - 1; i++)
            {
                Console.Write(vector[i] + ", ");
            }
            Console.WriteLine(vector[vector.Length - 1] + "]");
        }
        // Muestra por pantalla que botones de un candado estan apretados y cuales no.
        public void PushButtonPadlockSimulator()
        {
            bool[] botones = { false, false, false, false, false, false, false, false };
            int num;
            Console.WriteLine("Tienes 8 botones. Introduce números del 1 al 8 para activarlos y desactivarlos. Para acabar escribe -1");
            do
            {
                num = Convert.ToInt32(Console.ReadLine());
                if (num != -1)
                {
                    botones[num - 1] = !botones[num - 1];
                }
            } while (num != -1);

            Console.Write("[");
            for (int i = 0; i < botones.Length - 1; i++)
            {
                Console.Write(botones[i]+", ");
            }
            Console.WriteLine(botones[botones.Length - 1]+"]");
        }
        //Guardar el número de veces que se abre la caja.
        public void BoxesOpenedCounter()
        {
            int[] cajas = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            int num;
            Console.WriteLine("¿Que cajas se han abierto a lo largo del dia?");
            do
            {
                num = Convert.ToInt32(Console.ReadLine());
                if (num != -1)
                {
                    cajas[num]++;
                }

            } while (num != -1);

            Console.Write("[");
            for (int i = 0; i <= cajas.Length - 1; i++)
            {
                Console.Write(cajas[i]);
                if (i < cajas.Length)
                {
                    Console.Write(", ");
                }
            }
            Console.WriteLine("]");
        }
        // Te dice por pantalla el valor más pequeño de entre 10 números
        public void MinOf10Values()
        {
            int[] num = new int[10];
            int posicion1 = 0;
            int posicion2 = 1;
            int resultado = 0;
            for (int i = 0, posicionFrase = 1; i < 10; i++, posicionFrase++)
            {
                Console.WriteLine("Introduce el valor num " + posicionFrase + " del vector");
                num[i] = Convert.ToInt32(Console.ReadLine());
            }


            resultado = num[posicion1];

            for (int i = 0; posicion2 < num.Length - 1; i++, posicion1++, posicion2++)
            {
                if (resultado < num[posicion2])
                {
                    resultado = num[posicion1];
                }
                else if (resultado > num[posicion2])
                {
                    resultado = num[posicion2];
                }
            }
            Console.WriteLine("El número más pequeño de este vector es: " + resultado);


        }
        // Decir si hay números divisibles entre 7.
        public void IsThereAMultipleOf7()
        {
            int[] values = {4, 8, 9, 40, 54, 84, 40, 6, 84, 1, 1, 68, 84, 68, 4, 840, 684, 25, 40, 98,
                54, 687, 31, 4894, 468, 46, 84687, 894, 40, 846, 1681, 618, 161, 846, 84687, 6, 848};
            bool resultado = false;
            foreach (int val in values)
            {
                if (val % 7 == 0)
                {
                    resultado = true;
                }
            }
            Console.WriteLine(resultado);
        }
        // Pedir al usuario un conjunto de números y mirar si uno de los números que el me pida esta en el bucle.
        public void SearchInOrdered()
        {
            bool resultado = false;
            Console.WriteLine("¿Cuantos valores vas a introducir?");
            int numval = Convert.ToInt32(Console.ReadLine());
            int[] lista = new int[numval];

            int posicionlista = 1;
            if (numval > 0)
            {
                Console.WriteLine("Introduce los valores de menor a mayor.");
                Console.WriteLine("Introduce el valor num " + posicionlista + " del vector");
                lista[0] = Convert.ToInt32(Console.ReadLine());
                posicionlista = 2;

                for (int i = 1; i < 10; i++, posicionlista++)
                {
                    Console.WriteLine("Introduce el valor num " + posicionlista + " del vector");
                    do
                    {
                        lista[i] = Convert.ToInt32(Console.ReadLine());
                        if (lista[i] < lista[i - 1])
                        {
                            Console.WriteLine("ERROR: El número tiene que ser mayor que el anterior. \nPrueba otra vez");
                        }
                    } while (lista[i] < lista[i - 1]);
                }

                Console.WriteLine("¿Cuál es el número que quieres comprovar si esta en el bucle?");
                int num = Convert.ToInt32(Console.ReadLine());
                foreach (int val in lista)
                {
                    if (num == val)
                    {
                        resultado = true;
                    }
                }
                Console.WriteLine(resultado);
            }
        }
        // Introduce 10 vectores e imprimelos a la inversa
        public void InverseOrder()
        {
            Console.WriteLine("Introducd 10 números");
            int[] vector = new int[10];
            for (int i = vector.Length - 1; i >= 0; i--)
            {
                vector[i] = Convert.ToInt32(Console.ReadLine());
            }
            Console.Write("[");
            for (int i = 0; i < vector.Length - 1; i++)
            {
                Console.Write(vector[i] + ", ");
            }
            Console.WriteLine(vector[vector.Length - 1] + "]");
        }
        //Decir si la palabra es palindromo o no
        public void Palindrome()
        {
            Console.Write("Introduce una palabra: ");
            string palabra = Console.ReadLine();
            string palindromo = "";
            for (int i = palabra.Length - 1; i >= 0; i--)
            {
                palindromo += palabra[i];
            }
            if (palabra == palindromo)
            {
                Console.WriteLine("Es un palindromo!");
            }
            else
            {
                Console.WriteLine("No es un palindromo!");
            }
        }
        // Decir si la lista esta ordenada o desordenada
        public void ListSortedValues()
        {
            bool resultado = false;
            Console.WriteLine("¿Cuantos valores vas a introducir?");
            int numval = Convert.ToInt32(Console.ReadLine());
            int[] lista = new int[numval];

            int posicionlista = 1;
            if (numval > 0)
            {
                Console.WriteLine("Introduce los valores de menor a mayor.");
                Console.WriteLine("Introduce el valor num " + posicionlista + " del vector");
                lista[0] = Convert.ToInt32(Console.ReadLine());
                posicionlista = 2;

                for (int i = 1; i < numval; i++, posicionlista++)
                {
                    Console.WriteLine("Introduce el valor num " + posicionlista + " del vector");
                    lista[i] = Convert.ToInt32(Console.ReadLine());
                }

                for (int i = 1; i < numval; i++)
                {

                    if (lista[i + 1] > lista[i])
                    {
                        resultado = true;
                    }

                }
                Console.WriteLine(resultado);
            }

        }
        // Decir si la lista es cap i cua
        public void CapICuaValues()
        {
            bool respuesta = true;
            Console.WriteLine("¿Cuantos valores vas a introducir?");
            int numval = Convert.ToInt32(Console.ReadLine());
            int[] lista = new int[numval];

            int posicionlista = 1;
            if (numval > 0)
            {
                Console.WriteLine("Introduce los valores.");
                Console.WriteLine("Introduce el valor num " + posicionlista + " del vector");
                lista[0] = Convert.ToInt32(Console.ReadLine());
                posicionlista = 2;

                for (int i = 1; i < numval; i++, posicionlista++)
                {
                    Console.WriteLine("Introduce el valor num " + posicionlista + " del vector");
                    lista[i] = Convert.ToInt32(Console.ReadLine());
                }
            }


            int[] nuevalista = new int[numval];
            for (int i = lista.Length - 1, j = 0; i >= 0; i--, j++)
            {
                nuevalista[j] = lista[i];
            }

            for (int i = 0; i < numval; i++)
            {
                if (lista[i] != nuevalista[i])
                {
                    respuesta = false;
                }
            }

            if (respuesta == true)
            {
                Console.WriteLine("Es cap i cua.");
            }
            else
            {
                Console.WriteLine("No es cap i cua.");
            }
        }
        // Te dice si los 2 valores son iguales
        public void ListSameValues()
        {
            bool respuesta = true;
            Console.WriteLine("¿Cuantos valores tiene el primer vector?");
            int numval1 = Convert.ToInt32(Console.ReadLine());
            int[] lista1 = new int[numval1];

            int posicionlista = 1;
            if (numval1 > 0)
            {
                Console.WriteLine("Introduce los valores.");
                Console.WriteLine("Introduce el valor num " + posicionlista + " del vector");
                lista1[0] = Convert.ToInt32(Console.ReadLine());
                posicionlista = 2;

                for (int i = 1; i < numval1; i++, posicionlista++)
                {
                    Console.WriteLine("Introduce el valor num " + posicionlista + " del vector");
                    lista1[i] = Convert.ToInt32(Console.ReadLine());
                }
            }

            Console.WriteLine("¿Cuantos valores tiene el segundo vector?");
            int numval2 = Convert.ToInt32(Console.ReadLine());
            int[] lista2 = new int[numval2];

            posicionlista = 1;
            if (numval2 > 0)
            {
                Console.WriteLine("Introduce los valores.");
                Console.WriteLine("Introduce el valor num " + posicionlista + " del vector");
                lista2[0] = Convert.ToInt32(Console.ReadLine());
                posicionlista = 2;

                for (int i = 1; i < numval2; i++, posicionlista++)
                {
                    Console.WriteLine("Introduce el valor num " + posicionlista + " del vector");
                    lista2[i] = Convert.ToInt32(Console.ReadLine());
                }
            }

            if (numval1 != numval2)
            {
                respuesta = false;
            }

            for (int i = 0; i < numval1; i++)
            {
                if (lista1[i] != lista2[i])
                {
                    respuesta = false;
                }
            }

            if (respuesta == true)
            {
                Console.WriteLine("Los 2 vectores son iguales.");
            }
            else
            {
                Console.WriteLine("Los 2 vectores son diferentes.");
            }
        }
        // Suma todos los valores del vactor
        public void ListSumValues()
        {
            Console.WriteLine("¿Cuantos valores tiene el vector?");
            int numval = Convert.ToInt32(Console.ReadLine());
            int[] lista = new int[numval];
            int suma = 0;
            int posicionlista = 1;
            if (numval > 0)
            {
                Console.WriteLine("Introduce los valores.");
                Console.WriteLine("Introduce el valor num " + posicionlista + " del vector");
                lista[0] = Convert.ToInt32(Console.ReadLine());
                posicionlista = 2;

                for (int i = 1; i < numval; i++, posicionlista++)
                {
                    Console.WriteLine("Introduce el valor num " + posicionlista + " del vector");
                    lista[i] = Convert.ToInt32(Console.ReadLine());
                }
            }

            for (int i = 0; i < numval; i++)
            {
                suma += lista[i];
            }
            Console.WriteLine(suma);
        }
        // Te dice el valor de los productos con el IVA aplicado
        public void IvaPrices()
        {
            Console.WriteLine("¿Cuantos valores tiene el vector?");
            int numval = Convert.ToInt32(Console.ReadLine());
            double[] precios = new double[numval];
            int posicionlista = 1;
            if (numval > 0)
            {
                Console.WriteLine("Introduce los valores.");
                Console.WriteLine("Introduce el valor num " + posicionlista + " del vector");
                precios[0] = Convert.ToInt32(Console.ReadLine());
                posicionlista = 2;

                for (int i = 1; i < numval; i++, posicionlista++)
                {
                    Console.WriteLine("Introduce el valor num " + posicionlista + " del vector");
                    precios[i] = Convert.ToInt32(Console.ReadLine());
                }
            }

            for (int i = 0; i < precios.Length; i++)
            {
                Console.WriteLine(precios[i] + "IVA = " + (precios[i] + (precios[i] * 0.21)));
            }
        }
        // Muestra el crecimiento de la infección
        public void CovidGrowRate()
        {
            Console.WriteLine("¿Cuantas semanas vas a introducir?");
            int numsem = Convert.ToInt32(Console.ReadLine());
            int[] vector = new int[numsem];
            double[] infeccion = new double[numsem - 1];

            Console.WriteLine("Introduce el número de casos en cada semana: ");
            vector[0] = Convert.ToInt32(Console.ReadLine());
            for (int i = 1; i < numsem; i++)
            {
                vector[i] = Convert.ToInt32(Console.ReadLine());
                infeccion[i - 1] = Convert.ToDouble(vector[i]) / vector[i - 1];
            }
            Console.Write("[");
            for (int i = 0; i < infeccion.Length - 1; i++)
            {
                Console.Write(infeccion[i] + ", ");
            }
            Console.WriteLine(infeccion[infeccion.Length - 1] + "]");

        }
        // Imprime por pantalla los metros que recorre una bicicleta durante 10 segundos a cierta velocidad  
        public void BicicleDistance()
        {
            Console.WriteLine("¿A que velocidad en m/s va la bicicleta?");
            double num = Convert.ToDouble(Console.ReadLine());
            for (int i = 1; i <= 10; i++)
            {
                Console.Write(num * i);
            }
        }
        // Te dice que valor esta mas cerca de la media entre dichos valores
        public void ValueNearAvg()
        {
            Console.WriteLine("¿Cuantos números vas a introducir?");
            int numval = Convert.ToInt32(Console.ReadLine());
            int[] vector = new int[numval];
            int sum = 0;
            Console.WriteLine("Introduce " + numval + " números: ");
            for (int i = 0; i < vector.Length; i++)
            {
                vector[i] = Convert.ToInt32(Console.ReadLine());
            }
            foreach (int val in vector)
            {
                sum += val;
            }
            double media = sum / Convert.ToDouble(numval);
            double separacion;
            int respuesta = vector[0];
            foreach (int val in vector)
            {
                separacion = val - media;
                if (separacion < 0)
                {
                    separacion *= -1;
                }
                if (separacion < respuesta)
                {
                    respuesta = val;
                }
            }
            Console.WriteLine("El valor mas cercano a la media es "+ respuesta);
        }
        public void Isbn()
        {
            Console.WriteLine("Introduce los 10 números del código ISBN:");
            int[] isbn = new int[10];
            int resultado=0;
            bool valido = false;
            for (int i = 0; i < isbn.Length; i++)
            {
                isbn[i] = Convert.ToInt32(Console.ReadLine());
            }
            
            for (int i=1; i<isbn.Length; i++)
            {
                resultado += (isbn[i-1]*i);
            }
          
            if (resultado%11==isbn[9])
            {
                valido = true;
            }
            Console.WriteLine(valido);
        }
        public void Isbn13()
        {
            Console.WriteLine("Introduce los 13 números del código ISBN:");
            int[] isbn = new int[13];
            int sum = 0;
            bool valido = false;
            for (int i = 0; i < isbn.Length; i++)
            {
                isbn[i] = Convert.ToInt32(Console.ReadLine());
            }
            
            for (int i =0; i < isbn.Length-1; i++)
            {
                if (i % 2 == 1)
                {
                    isbn[i] *= 3;
                }
            }


            for (int i = 0; i < isbn.Length; i++)
            {
                sum += isbn[i];
            }

            

            if (sum % 10 == 0)
            {
                valido = true;
            }
            Console.WriteLine(valido);
        }
        public bool Menu(bool end)
        {
            string[] exercicis = { " 0 - Exit", " 1 - DayOfWeek", " 2 - PlayerNumbers", " 3 - CandidatesList",
                " 4 - LetterInWord", " 5 - AddValuesToList", " 6 - Swap", " 7 - PushButtonPadlockSimulator",
                " 8 - BoxesOpenedCounter", " 9 - MinOf10Values", "10 - IsThereAMultipleOf7", "11 - SearchInOrdered",
                "12 - InverseOrder", "13 - Palindrome", "14 - ListSortedValues", "15 - CapICuaValues",
                "16 - ListSameValues", "17 - ListSumValues", "18 - IvaPrices", "19 - CovidGrowRate",
                "20 - BicicleDistance", "21 - ValueNearAvg", "22 - Isbn", "23 - Isbn13"
            };
            Console.WriteLine("Quin exercici vols executar?\n");
            for (int i = 0; i < 24; i++)
            {
                Console.WriteLine(exercicis[i]);
            }
            Console.Write("\n Escriu un número: ");
            string option = Console.ReadLine();
            switch (option)
            {
                case "0":
                    end = !end;
                    break;
                case "1":
                    DayOfWeek();
                    break;
                case "2":
                    PlayerNumbers();
                    break;
                case "3":
                    CandidatesList();
                    break;
                case "4":
                    LetterInWord();
                    break;
                case "5":
                    AddValuesToList();
                    break;
                case "6":
                    Swap();
                    break;
                case "7":
                    PushButtonPadlockSimulator();
                    break;
                case "8":
                    BoxesOpenedCounter();
                    break;
                case "9":
                    MinOf10Values();
                    break;
                case "10":
                    IsThereAMultipleOf7();
                    break;
                case "11":
                    SearchInOrdered();
                    break;
                case "12":
                    InverseOrder();
                    break;
                case "13":
                    Palindrome();
                    break;
                case "14":
                    ListSortedValues();
                    break;
                case "15":
                    CapICuaValues();
                    break;
                case "16":
                    ListSameValues();
                    break;
                case "17":
                    ListSumValues();
                    break;
                case "18":
                    IvaPrices();
                    break;
                case "19":
                    CovidGrowRate();
                    break;
                case "20":
                    BicicleDistance();
                    break;
                case "21":
                    ValueNearAvg();
                    break;
                case "22":
                    Isbn();
                    break;
                case "23":
                    Isbn13();
                    break;
                default:
                    Console.WriteLine("\nOpció incorrecta!\n");
                    break;
            }
            Console.WriteLine("\n\n[PRESS ENTER TO CONTINUE]");
            Console.ReadLine();
            Console.Clear();
            return end;
        }
        static void Main()
        {
            var menu = new Vectors();
            bool end = false;
            while (!end) { end = menu.Menu(end); }
        }
    }
}
       